# hkavr

Harmann & Kardon Audio/Video Receiver (AVR) RS232 interface.

## Installing the module

Easy! With [npm](http://npmjs.org/):

	npm install hkavr

Now you can include the module in your project.

	// AVR265 module
	var model 		= '265';

	// webservice port configuration
	var servicePort 	= 8265;

	// Raspberry configuration for SerialPort
	var serialInterface 	= '/dev/ttyAMA0';
	var serialOption 	= {
				    baudrate: 57600,
				    databits: 8,
				    parity: "none",
				    stopbits: 1,
				    flowControl: false
				};

	// initialisation
	var server = require('hkavr');
	server.avr(model,servicePort,serialInterface,serialOption);

## Changelog

Version 0.1.8

* response return available & humanized

Version 0.1.7

* http server replaced by socket.io

Version 0.1.6

* add Treble & Bass specific value command (not working, waiting response from h&k support)

Version 0.1.5

* add FM & AM station frequency command

Version 0.1.4

* add webservice port configuration at initialisation
* add serial port configuration at initialisation

## Note

Still in development.

Available(s) driver(s) for the following serie(s):

* AVR265 

* need good user permissions on the port

## Use

The module start a simple socket service on the Raspberry connected to the AVR RS232 port.

## Manual

http://stationweb.fr/node-hkavr (only french version for the moment) 

## Development in progress & todo

* Add inline command interface with arguments for direct command (no server)
* Test on Linux, Apple, Windows
* Extend module for other series (AVR 2650, AVR 365, AVR 3650, AVR 635, AVR 630 , AVR 435 ,AVR 430)

## License

Copyright (c) 2013 Vincent Laag <vincent@stationweb.fr>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
