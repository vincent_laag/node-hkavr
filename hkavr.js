// hkavr.js
"use strict" 

// Copyright 2013 Vincent Laag <vincent@stationweb.fr>

var http 	= require('http');
var url 	= require('url');
var serialport = require("serialport");
var SerialPort = serialport.SerialPort; 
require('buffertools');

var avr = function(model,servicePort,serialInterface,serialOptions) {
  
	var AVR 		= require('./lib/avr'+model);
	var pcsend 		= '504353454E44';
	var pcdatatype 		= '02';
	var pcdatalength 	= '04';
	var sp 			= new SerialPort(serialInterface, serialOptions);
	var io 			= require('socket.io').listen(servicePort);
	var temp 		= '';
	
	sp.on("open", function () {
	  
	  temp = '';
	  
	  /* emit humanized AVR output */
	  sp.on('data', function(data) {
	    
	    var buf = new Buffer(data);
	    temp += buf.toHex();
	    
	    if(temp.length == 20 || temp.length == 22) {
	      
	      var output = AVR.avrResponse(temp);
	      io.sockets.emit('AVR', { output: output });
	      temp = '';
	    } 
	    if(temp.length > 30) { // prevent non interpreted response
	      temp = '';
	    }
	  });
	  
	}); 
	
	io.sockets.on('connection', function (socket) {

	  socket.on('avrcmd', function (son) {
		if(son.value !== undefined) {
		  var hexaCmd = AVR.avrCommandValue(son.cmd,son.value);
		} else {
		  var hexaCmd = AVR.avrCommand(son.cmd);
		}
		if ( hexaCmd !== false) {
		  var bufSend = new Buffer( pcsend + pcdatatype + pcdatalength + hexaCmd ).fromHex();
		  sp.write(bufSend); 
		}
	  });
	  
	  socket.on('disconnect', function () {
	    temp = '';
	  });
	  
	});
	
	
}
exports.avr 	= avr;